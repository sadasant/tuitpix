// tuitpix/ui.js
// Copyright (c) 2013 Daniel Rodriguez
// MIT License [http://www.opensource.org/licenses/mit-license.php]
//
// User interface wrappers.
//

/* global tuitpix */
tuitpix.ui = function() {
    "use strict";

    var tuitpix = this;
    var ui      = tuitpix.ui = {};
    var $       = tuitpix.lib.$;
    var jscolor = tuitpix.lib.jscolor;

    // jQuery elements
    ui._classes = {};
    ui.window   = $(window);
    ui.document = $(document);
    ui.avatar   = $("#avatar");
    ui.canvas   = $("#avatar canvas");

    // Export button
    ui.export = ui.avatar.find("#export");

    // Save button
    ui.save = ui.avatar.find("#save");

    // Import button
    ui.import = ui.avatar.find("#import");

    // Menu
    ui.menu        = $("#menu");
    ui.menu_body   = ui.menu.find("#menu-body");
    ui.menu_outfit = ui.menu.find("#menu-outfit");
    ui.menu_hair   = ui.menu.find("#menu-hair");
    ui.menu_beard  = ui.menu.find("#menu-beard");
    ui.menu_addons = ui.menu.find("#menu-addons");
    ui.menu_saved  = ui.menu.find("#menu-saved");
    ui.menu_maker  = ui.menu.find("#menu-maker");
    ui._classes.menu_active = "active";

    ui.first_render = false;
    ui.menu.find("li:not(li li)").click(function() {
        var active  = ui._classes.menu_active;
        var unmaker = false;
        ui.menu.find("li").removeClass(active);
        ui.menu.find("ul:visible").hide();
        ui.menu.find("span:visible").hide();
        var $this = $(this);
        $this.addClass(active);
        $this.find("span").show();
        $this.find("ul").show();
        // Download the bodies
        var feature = this.id.substr(5);
        if (~feature.indexOf("maker")) {
            ui.import.show();
            tuitpix.maker.activate();
        } else
        if (tuitpix.maker.is_active) {
            ui.import.hide();
            tuitpix.maker.deactivate();
            unmaker = true;
        }
        tuitpix.avatar.downloadFeatures(feature, function() {
            if (unmaker) {
                unmaker = false;
                // Let's click the body's default option.
                ui.options.find("."+tuitpix.avatar.selected.body).click();
            }
            if (!ui.first_render || tuitpix.maker.is_active) {
                ui.first_render = true;
                // Draw the selected settings
                ui.options_minis[0].$element.click();
                // If the option is the maker, let's set
                // all the maker properties.
                if (tuitpix.maker.is_active) {
                    ui.option_new_option.appendTo(ui.options);
                    ui.option_new_option.click(ui._newOptionClick);
                }
            }
        });
    });

    // Sub menues
    ui.menu.find("li li").click(function(e) {
        e.stopPropagation();
        var active = ui._classes.menu_active;
        ui.menu.find("li").removeClass(active);
        var $this = $(this);
        $this.addClass(active);
        // Download the bodies
        var feature = this.id.substr(5);
        tuitpix.avatar.downloadFeatures(feature);
    });

    // Options
    ui.options = $("#options");
    ui._classes.option_active = "active";
    ui.new_option = $("<div/>").addClass("option");
    ui.new_option_empty = ui.new_option.clone();
    ui.new_option.append($("<div/>").addClass("three-dots"));

    // option_new_option is the button that is used to create
    // new features in the maker.
    ui.option_new_option = ui.new_option.clone();
    ui.option_new_option.html("").addClass("new-option").append($("<div/>").addClass("cross"));
    ui._newOptionClick = function() {
        tuitpix.maker.cloneDefault(this);
    };

    ui.options_minis = [];

    ui._clearOptions = function() {
        ui.options_minis = [];
        ui.options_minis.length = 0;
        ui.options.html("");
    };

    ui._newOption = function(silent) {
        var option = ui.new_option_empty.clone();
        var mini   = tuitpix.canvas.newMini();
        option.click(function(e) {
            e.stopPropagation();
            var active = ui._classes.option_active;
            ui.options.find(".option").removeClass(active);
            option.addClass(active);
            var feature = tuitpix.avatar.last_feature;
            if (feature === "bodytype") {
                // Get the body instead of the body type...
                tuitpix.avatar.selected.body = tuitpix.avatar.features.body[option.index()];
            } else {
                // Get the feature based on the option's index in the option's ul element.
                tuitpix.avatar.selected[feature] = option.index();
                // Let's make sure all related elements are cleared
                var cropped_feature = feature.replace(/-.*/, "");
                for (var k in tuitpix.avatar.selected) {
                    if (k !== feature && ~k.indexOf(cropped_feature)) {
                        tuitpix.avatar.selected[k] = undefined;
                    }
                }
            }
            tuitpix.avatar.drawSelected();
        });
        mini.appendTo(option);
        option.data("mini", ui.options_minis.length);
        if (!silent) {
            ui.options_minis.push(mini);
            ui.options.append(option);
        }
        return {
            $element : option,
            element  : option[0],
            mini     : mini
        };
    };

    // Colors
    ui.colors    = $("#colors");
    ui.new_color = $("<li/>").addClass("color");
    ui._classes.color_active = "active";
    ui._classes.color_picked = "picked";

    // color_new_color is the button that is used to create
    // new colors in the maker.
    ui.color_new_color = ui.new_color.clone().addClass("new-color");
    // _newColorClick unactives the current actives or picked colors
    // and generates a new color, which is inserted before the color_new_color button.
    var new_colors = {};
    ui._newColorClick = function() {
        var active = ui._classes.color_active;
        var picked = ui._classes.color_picked;
        ui.colors.find("li."+active).removeClass(active).html("");
        ui.colors.find("li."+picked).removeClass(picked);
        ui.color_eraser.removeClass(active);
        tuitpix.maker.eraser = false;
        // We need to check if the color is already made
        var color = tuitpix.utils.newColor();
        var str_color = "rgb(" + color.rgb.join(", ") + ")"; // You can test this with: "rgb(218, 196, 158)";
        var $found = ui.colors.find("li").filter(function() {
            return this.style.background === str_color;
        });
        if ($found.length) {
            $found.addClass(picked);
            return;
        }
        var new_color = ui._newColor(color.hex, true);
        new_color.addClass("picked");
        new_color.insertBefore(ui.color_new_color);
    };

    // The color_eraser is the eraser tool in the maker.
    ui.color_eraser = ui.new_color.clone().addClass("eraser");
    // When clicked, it unactivates the current active or picked colors,
    // sets itself as active and sets the tuitpix.maker.eraser flag to true.
    ui._eraserClick = function() {
        var active = ui._classes.color_active;
        var picked = ui._classes.color_picked;
        ui.colors.find("li."+active).removeClass(active).html("");
        ui.colors.find("li."+picked).removeClass(picked);
        ui.color_eraser.addClass(active);
        tuitpix.maker.eraser = true;
    };

    ui.$jscolor = $("<input/>").addClass("color");
    ui.jscolor  = new jscolor.color(ui.$jscolor[0], {});

    ui._clearColors = function() {
        ui.colors.html("");
    };

    // _newColor clones a color from the stored virgin new_color object,
    // sets it as active, sets the given background color
    // and binds the color element on click to show the jscolor picker view,
    // binding the jscolor picker changes onclick.
    ui._newColor = function(background, do_not_append) {
        var color  = ui.new_color.clone();
        var active = ui._classes.color_active;
        color.data("color", background);
        color.css("background", "#"+background);
        color.click(function(e) {
            // To make sure the input doesn't close itself
            // if we click the active color again.
            if (color.hasClass(active)) return;
            var picked = ui._classes.color_picked;
            // If the maker is active, we have to un-pick
            // all the picked colors and pick the clicked one,
            // also, deactivating the eraser.
            if (tuitpix.maker.is_active) {
                ui.colors.find("li."+picked).removeClass(picked);
                color.addClass(picked);
                tuitpix.maker.eraser = false;
            }
            ui.colors.find("li."+active).removeClass(active).html("");
            color.addClass(active);
            ui.jscolor.fromString(background);
            color.append(ui.$jscolor);
            ui.jscolor.showPicker();
            // When the jscolor changes, we have to set the color's
            // background as the changes impose, also, we'll change
            // the avatar's colors as requested.
            ui.$jscolor.on("change", function() {
                var value  = "#"+this.value;
                background = value;
                color.css("background", value);
                color.data("color", value);
                tuitpix.avatar.changeColor(value, $(this.parentNode).index());
            });
        });
        if (!do_not_append) {
            ui.colors.append(color);
        }
        return color;
    };

    ui._appendReset = function() {
        var $reset = $("<div class='resetColors'/>");
        $reset.html("<span>Reset Colors</span>");

        $reset.click(function() {
            var avatar   = tuitpix.avatar;
            var body     = avatar.selected.body;
            var feature  = avatar.last_feature;
            var selected = avatar.features[feature][body][avatar.selected[feature]];
            var backup   = avatar.features_backup[feature][body][avatar.selected[feature]];
            for (var i = 0; i < selected.length; i++) {
                if (feature === "bodytype") {
                    avatar.changed_skin_colors[backup[i][0].toUpperCase()].color = backup[i][0];
                }
                selected[i] = backup[i].slice();
            }
            avatar.drawSelected();
        });

        ui.colors.append($reset);
    };

};
