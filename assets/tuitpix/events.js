// tuitpix/events.js
// Copyright (c) 2013 Daniel Rodriguez
// MIT License [http://www.opensource.org/licenses/mit-license.php]
//
// Tuitpix events.
// Here we will manage the scrolling functionalities of the options
// sidebar, as well as any other event.
//

/* global tuitpix */
tuitpix.events = function() {
    "use strict";

    var tuitpix = this;
    var events  = tuitpix.events = {};
    var $       = tuitpix.lib.$;

    // Options Scrolling
    // -----------------

    tuitpix.ui.options.slimscroll({
        height   : tuitpix.ui.window.height(),
        position : "left",
        distance : "415px",
        size     : "5px",
        color    : "#111"
    });

    // On window resize
    tuitpix.ui.window.resize(function() {
        clearTimeout($.data(this, "resizeTimer"));
        $.data(this, "resizeTimer", setTimeout(onWindowResize, 250));
    });
    function onWindowResize() {
        var new_height = tuitpix.ui.window.height();
        // Update options' scroll
        tuitpix.ui.options.css("height", new_height);
        tuitpix.ui.options.parent().css("height", new_height);
        // Update maker offset
        if (tuitpix.maker.is_active) {
            tuitpix.maker.offset = tuitpix.ui.canvas.offset();
            tuitpix.maker.width  = tuitpix.ui.canvas.width();
            tuitpix.maker.height = tuitpix.ui.canvas.height();
            tuitpix.maker.offset.right  = tuitpix.maker.offset.left + tuitpix.maker.width;
            tuitpix.maker.offset.bottom = tuitpix.maker.offset.top  + tuitpix.maker.height;
        }
    }

    // On Mouse Move

    events.cursor = {
        x : 0,
        y : 0
    };

    function onMouseMove(e) {
        var X;
        if (document.all && event.clientX) {
            X  = event.clientX;
            X += document.documentElement.scrollLeft || document.body.scrollLeft;
        } else
        if (e.pageX) {
            X = e.pageX;
        }
        var Y;
        if (document.all && event.clientY) {
            Y  = event.clientY;
            Y += document.documentElement.scrollTop || document.body.scrollTop;
        } else
        if (e.pageY) {
            Y = e.pageY;
        }
        events.cursor.x = X;
        events.cursor.y = Y;
        if (tuitpix.maker.is_active && events.cursor.down) {
            tuitpix.maker.mousePaint();
        }
    }

    events.catchMousemove = function() {
        document.onmousemove = onMouseMove;
        document.onmousedown = function (e) {
            events.cursor.down = true;
        };
        document.onmouseup = function (e) {
            events.cursor.down = false;
        };
    };

    events.leaveMousemove = function() {
        document.onmousemove = undefined;
        document.onmousedown = undefined;
        document.onmouseup   = undefined;
    };

    // Hide Colors
    tuitpix.ui.document.mouseup(function(e) {
        var $target = $(e.target);
        if (!(tuitpix.ui.$jscolor.is(e.target) ||
        $target.parent().hasClass("colorPicker"))) {
            tuitpix.ui.jscolor.hidePicker();
            tuitpix.ui.$jscolor
            .parent()
            .html("")
            .removeClass(tuitpix.ui._classes.color_active);
        }
    });

};
