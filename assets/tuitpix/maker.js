// tuitpix/maker.js
// Copyright (c) 2013 Daniel Rodriguez
// MIT License [http://www.opensource.org/licenses/mit-license.php]
//
// Where all the general-use functions are created.
//

/* global tuitpix */
tuitpix.maker = function() {
    "use strict";

    var tuitpix = this;
    var maker   = tuitpix.maker = {};
    var store   = tuitpix.lib.store;
    var $       = tuitpix.lib.$;

    maker.is_active = false;
    maker.eraser    = false;
    maker.offset    = tuitpix.ui.canvas.offset();
    maker.width     = tuitpix.canvas.pixel.width  * tuitpix.canvas.side_pixels;
    maker.height    = tuitpix.canvas.pixel.height * tuitpix.canvas.side_pixels;
    maker.offset.right  = maker.offset.left + maker.width;
    maker.offset.bottom = maker.offset.top  + maker.height;

    maker.mousePaint = function(e) {
        if (e && e.button !== 0) return;
        var cursor = tuitpix.events.cursor;
        if (!(cursor.x || cursor.y)) return;
        // Out of Range
        if (cursor.x < maker.offset.left || cursor.x > maker.offset.right ||
            cursor.y < maker.offset.top  || cursor.y > maker.offset.bottom) {
            return;
        }
        var pixel = tuitpix.canvas.pixel;
        var x = cursor.x - maker.offset.left + 35;
        var y = cursor.y - maker.offset.top  + 8;
        var color = tuitpix.ui.colors.find(".picked").data("color") || "clear";
        var brush = [color, []];
        var pos   = Math.floor((x-pixel.width)/pixel.width);
        pos += tuitpix.canvas.side_pixels *(Math.floor(y/pixel.height) - 1);
        var obj   = [pos,1,1];
        console.log(pos + ", \"" + color + "\"");
        brush[1].push(obj);
        var selected = tuitpix.avatar.features.maker[tuitpix.avatar.selected.body][tuitpix.avatar.selected.maker];
        var found_color = false;
        // TODO: we will normalize the object on save
        selected.push(brush);
        tuitpix.canvas.draw(brush, undefined);
    };

    maker.activate = function() {
        maker.is_active = true;
        tuitpix.ui.avatar.addClass("maker");
        tuitpix.ui.avatar.click(maker.mousePaint);
        tuitpix.events.catchMousemove();
        tuitpix.ui._clearOptions();
        tuitpix.ui._clearColors();
        var option = tuitpix.ui._newOption();
        maker.updateActiveMini(option);
        tuitpix.ui.save.unbind("click");
        tuitpix.ui.save.click(maker.save);
    };

    maker.deactivate = function() {
        maker.is_active = false;
        tuitpix.ui.avatar.removeClass("maker");
        tuitpix.ui.avatar.unbind("click");
        tuitpix.events.leaveMousemove();
        tuitpix.ui._clearOptions();
        tuitpix.ui._clearColors();
        clearInterval(intervalActiveMini);
        tuitpix.ui.save.unbind("click");
    };

    var intervalActiveMini = null;
    maker.updateActiveMini = function(option) {
        // Replace selected with parsed drawn object
        intervalActiveMini = setInterval(function() {
            // I have to make the function that makes the JSON from the canvas.
            var index = tuitpix.ui.options.find(".active").index();
            var mini  = tuitpix.ui.options_minis[index];
            if (!mini) return;
            mini.clear();
            var selected = tuitpix.avatar.features.maker[tuitpix.avatar.selected.body][tuitpix.avatar.selected.maker];
            mini.draw(selected);
        }, 1000);
    };

    maker.cloneDefault = function(option_new_option) {
        var body = tuitpix.avatar.selected.body;
        var selected = tuitpix.avatar.features.bodytype[body][0];
        var option = tuitpix.ui._newOption();
        selected = $.extend(true, [], selected);
        tuitpix.avatar.features.maker[body].push(selected);
        option.mini.draw(selected);
        option.$element.insertBefore(option_new_option);
    };

    // It would be interesting if we saved images to the server,
    // they weigh less than our string objects.
    maker.save = function() {
        var index = tuitpix.ui.options.find(".active").index();
        var mini  = tuitpix.ui.options_minis[index];
        if (!mini) return;
        var data = mini.getData().data;
        console.log(data);
    };

};
