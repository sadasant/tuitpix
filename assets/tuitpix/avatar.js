// tuitpix/avatar.js
// Copyright (c) 2013 Daniel Rodriguez
// MIT License [http://www.opensource.org/licenses/mit-license.php]
//
// Where all the general-use functions are created.
//

/* global tuitpix */
tuitpix.avatar = function() {
    "use strict";

    var tuitpix = this;
    var avatar  = tuitpix.avatar = {};
    var store   = tuitpix.lib.store;
    var $       = tuitpix.lib.$;

    // Avatar features
    avatar.features = {
        skin     : null, // Detailed below.
        bodies   : null,
        bodytype : null, // The first feature to download, comes with the bodies.
        outfit   : null,
        hair     : null,
        beard    : null,
        addons   : null,
        saved    : null,
        maker    : null  // Maker is filled initially with bodytype data.
    };

    // Avatar features backup
    avatar.features_backup = {
        skin     : null, // Detailed below.
        bodies   : null,
        bodytype : null, // The first feature to download, comes with the bodies.
        outfit   : null,
        hair     : null,
        beard    : null,
        addons   : null,
        saved    : null,
        maker    : null  // Maker is filled initially with bodytype data.
    };

    // Avatar skins
    // Skins alter randomly on load.
    avatar.features.skins = [
        [ "#DAC49E", "#AF9D7E", "#9E8860" ],
        [ "#AD8B62", "#806748", "#231e1b" ],
        [ "#CDA57B", "#B8946E", "#3E2115" ],
        [ "#EEC9A2", "#C9A988", "#B87A38" ],
        [ "#3C332B", "#302C28", "#212121" ]
    ];

    // Making the order list, based on the menu :)
    avatar.order = tuitpix.ui.menu.find("li").map(function(i,e){
        if (e.id === "menu-body") return "bodytype";
        return e.id.replace("menu-","");
    }).filter(function(i, e) {
        if (e && e !== "saved") {
            return e;
        }
    });

    // Avatar selected values
    avatar.selected = {
        skin     : 0,
        body     : "m",
        bodytype : 0,
        outfit   : 0,
        hair     : 0,
        beard    : 0,
        addons   : 0,
        saved    : 0
    };

    // Avatat fill features
    avatar.last_feature = "";
    avatar.downloadFeatures = function(feature, callback, silent) {
        // var stored_features = store.get("tuitpix-features");
        // var date = new Date();
        var download = true;
        // if (stored_features) {
        //     // TODO:
        //     // download = false if outdated.
        //     console.log(stored_features, date);
        // }
        var original_feature = feature.replace("general-", "");
        feature = feature === "body" ? "bodytype" : feature;
        if (!silent && avatar.features[feature]) {
            done(avatar.features[feature]);
            return;
        }
        if (download) {
        // --- DEVELOPMENT ---
            var date = (new Date()).getTime();
            $.get("assets/px/"+original_feature, { date : date }, function(data) {
        // --- DEVELOPMENT ---
        // --- PRODUCTION ---
        //  $.post("assets/px/"+original_feature, { XSRF : XSRF }, function(data) {
        //      data = atob(data);
        // --- PRODUCTION ---
                data = tuitpix.utils.parseData(data);
                avatar.features[feature] = data;
                avatar.features_backup[feature] = window.jQuery.extend(true, {}, data);
                if (feature === "bodytype") {
                    // Clone the bodies
                    avatar.features.maker = $.extend(true, {}, data);
                }
                done(data);
            });
        }
        function done(data) {
            var k;
            var i;
            var option;
            var body_data;
            var fill = false;
            if (!silent) {
                tuitpix.ui._clearOptions();
                avatar.last_feature = feature;
            }
            if (original_feature === "body") {
                if (!avatar.features.body) {
                    avatar.features.body = [];
                    fill = true;
                }
                for (k in data) {
                    if (fill) avatar.features.body.push(k);
                    if (!silent) {
                        option = tuitpix.ui._newOption();
                        // Storing the body so we can find it later...
                        option.$element.addClass(k);
                        option.mini.draw(data[k][0]);
                    }
                }
            } else {
                body_data = data[avatar.selected.body];
                if (!silent) {
                    for (i = 0; i < body_data.length; i++) {
                        option = tuitpix.ui._newOption();
                        option.mini.draw(body_data[i], avatar, original_feature);
                    }
                }
            }
            if (callback) callback();
        }
    };

    // Draw Avatar selected features
    avatar.drawSelected = function(silent) {
        var selected;
        var skin = avatar.selected.skin;
        var body = avatar.selected.body;
        var feature;
        tuitpix.canvas.clear();
        for (var k, i = 0; i < avatar.order.length; i++) {
            k = avatar.order[i];
            if (!avatar.features[k]) {
                avatar.downloadFeatures(k, avatar.drawSelected, true); // Silent
                continue;
            } else
            if (!(avatar.features[k][body] && avatar.features[k][body][avatar.selected[k]])) {
                continue;
            }
            // Ignore everything but maker when is active.
            if (k !== "maker" && tuitpix.maker.is_active) continue;
            // Ignore maker when it's not active.
            if (k === "maker" && !tuitpix.maker.is_active) continue;
            selected = avatar.features[k][body][avatar.selected[k]];
            if (k === avatar.last_feature) {
                feature = selected;
            }
            if (k !== "bodytype") {
                tuitpix.canvas.draw(selected, undefined, undefined, avatar.changed_skin_colors);
            } else {
                tuitpix.canvas.draw(selected);
            }
            // Update active mini
            // TODO: Make this faster? Maybe cache the active element
            tuitpix.ui.options_minis[tuitpix.ui.options.find(".active").index()].draw(selected);
            // // Rainbow
            // tuitpix.canvas.draw(selected, undefined, true);
        }
        if (silent) return;
        if (!feature) return;
        // Add colors
        tuitpix.ui._clearColors();
        for (i = 0; i < feature.length; i++) {
            tuitpix.ui._newColor(feature[i][0]);
        }
        if (tuitpix.maker.is_active) {
            tuitpix.ui.color_new_color.appendTo(tuitpix.ui.colors);
            tuitpix.ui.color_new_color.click(tuitpix.ui._newColorClick);
            tuitpix.ui.colors.find(".color:not(.new-color)").last().addClass(tuitpix.ui._classes.color_picked);
            // tuitpix.ui.color_new_color.click();
            tuitpix.ui.color_eraser.appendTo(tuitpix.ui.colors);
            tuitpix.ui.color_eraser.click(tuitpix.ui._eraserClick);
        }
        tuitpix.ui._appendReset();
    };

    // Change color
    avatar.changed_skin_colors = {};
    avatar.changeColor = function(_color, index) {
        var body     = avatar.selected.body;
        var feature  = avatar.last_feature;
        var selected = avatar.features[feature][body][avatar.selected[feature]];
        var color    = selected[index][0];
        if (_color[0] === "#") _color = _color.slice(1);
        var i, k;
        for (i = 0; i < selected.length; i++) {
            if (selected[i][0] === color) {
                selected[i][0] = _color;
            }
        }
        if (feature === "bodytype") {
            for (k in avatar.changed_skin_colors) {
                if (avatar.changed_skin_colors[k].i === index) {
                    avatar.changed_skin_colors[k].color = _color;
                }
            }
            avatar.changed_skin_colors[color.toUpperCase()] = { i : index, color : _color };
        }
        avatar.drawSelected(true);
    };

    // Export image
    avatar.export = function() {
        window.open(tuitpix.canvas.element.toDataURL("image/png"));
    };
    tuitpix.ui.export.click(avatar.export);

    // Save image
    avatar.save = function() {
    };
    tuitpix.ui.save.click(avatar.save);

    // Import image
    var file_uploader = $("<input/>").attr({ type : "file" });
    var file_mini;
    avatar.import = function(e) {
        var reader = new FileReader();
        reader.onload = function(event){
            var img = new Image();
            img.onload = function(){
                // If the image is bigger than what we care to read,
                // then show an error or something!
                if (img.width > tuitpix.canvas.side_pixels || img.height > tuitpix.canvas.side_pixels) {
                    console.log(img.width, img.height);
                    return;
                }
                file_mini.element.width  = img.width;
                file_mini.element.height = img.height;
                file_mini.context.drawImage(img, 0, 0, img.width, img.height);
                var obj = file_mini.getData().obj;
                // console.log(obj);
                var body = tuitpix.avatar.selected.body;
                avatar.features.maker[body].push(obj);
                var option = tuitpix.ui._newOption(true);
                tuitpix.ui.options_minis.push(option.mini);
                option.mini.draw(obj);
                option.$element.insertBefore(tuitpix.ui.option_new_option);
                option.$element.click();
            };
            img.src = event.target.result;
        };
        reader.readAsDataURL(e.target.files[0]);
    };
    file_uploader.on("change", avatar.import);
    tuitpix.ui.import.click(function() {
        // Here we should provide details on how the imported image
        // should be, in size, etc.
        file_mini = tuitpix.canvas.newMini();
        file_uploader.click();
    });

};
