// tuitpix/utils.js
// Copyright (c) 2013 Daniel Rodriguez
// MIT License [http://www.opensource.org/licenses/mit-license.php]
//
// Where all the general-use functions and values are created.
// Currently we have some mathematically useful objects (numbers, strings),
// as well as some functions to translate rgb(0,0,0) syntax into hex #000.
//

/* global tuitpix */
tuitpix.utils = function() {
    "use strict";

    var tuitpix = this;
    var utils = tuitpix.utils = {};

    // Some pre-declared values
    utils.values = {};
    var two55    = utils.values.two55 = 255;
    var hash     = "#";
    var zero     = "0";

    utils.RGBtoHex = function(r, g, b) {
        if (r > two55 || g > two55 || b > two55) {
            throw "Invalid rgb";
        }
        var hex = ((r << 16) | (g << 8) | b).toString(16);
        // If black.
        while (hex.length < 6) {
            hex = zero + hex;
        }
        return hex;
    };

    // Generates random pastel colors for 118 users, before repeating.
    var generated_colors = {};
    var total_colors     = 0;
    var hue_low          = 90;
    var hue_substraction = 0;
    var golden_substract = 7.6388; // 137.5 deg of twenty.
    var restart          = false;
    utils.newColor = function(){
        if (total_colors && total_colors === 59) {
            if (restart) {
                generated_colors = {};
                hue_low          = 90;
                hue_substraction = 0;
                restart          = false;
            } else {
                hue_low -= hue_substraction;
                restart = true;
            }
            total_colors = 0;
        }
        hue_substraction    = hue_low - golden_substract * Math.floor(total_colors / 5);
        var pastel_hue_low  = hue_substraction;
        var pastel_hue_high = 196;
        var pastel_hue_int  = Math.floor(Math.random()*6) + 1;  // 6 possible pastle colors
        var pastel_hue_byte = pastel_hue_int.toString(2);       // Converted to byte: 001, 010, 011, 100, 101, 110
        var color = [];
        if (pastel_hue_byte.length === 2) {
            pastel_hue_byte = "0" + pastel_hue_byte;
        }
        if (pastel_hue_byte.length === 1) {
            pastel_hue_byte = "00" + pastel_hue_byte;
        }
        for (var i = 0; i < 3; i++) {
            if (pastel_hue_byte[i] === "0") {
                color.push(pastel_hue_low);
            } else {
                color.push(pastel_hue_high);
            }
        }
        var hex_color = utils.RGBtoHex(color[0], color[1], color[2]);
        if (generated_colors[hex_color]) {
            return utils.newColor(); // Continue checking...
        }
        generated_colors[hex_color] = true;
        total_colors++;
        return {
            rgb : color,
            hex : hex_color
        };
    };

    utils.parseData = function(data) {
        data = data.split("\n").filter(function(l) {
            // Filtering comments
            return l[0] !== "#";
        });
        var result = {};
        var line;
        var type;
        var last;
        var part;
        var last_p;
        var pos, x, y;
        for (var i = 0; i < data.length - 1; i++) {
            line = data[i];
            if (line.length === 1) {
                // Part type
                type = line;
                result[type] = [];
            }
            if (line.length < 2) {
                last = result[type].length;
                result[type][last] = [];
                continue;
            }
            line = line.split(" ");
            for (var j = 0; j < line.length; j++) {
                part = line[j];
                if (part.indexOf(",") === -1) {
                    // Hex Color
                    last_p = result[type][last].length;
                    result[type][last][last_p] = [part];
                    continue;
                }
                // Position
                part = part.split(",");
                pos  = parseInt(part[0], 10);
                x    = part[1] ? parseInt(part[1], 10) : 1;
                y    = part[2] ? parseInt(part[2], 10) : 1;
                result[type][last][last_p].push(pos,x,y);
            }
        }
        return result;
    };

};
