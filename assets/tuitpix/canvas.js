// tuitpix/canvas.js
// Copyright (c) 2013 Daniel Rodriguez
// MIT License [http://www.opensource.org/licenses/mit-license.php]
//
// Where all the general-use functions are created.
//

/* global tuitpix */
tuitpix.canvas = function() {
    "use strict";

    var tuitpix = this;
    var canvas  = tuitpix.canvas = {};
    var $       = tuitpix.lib.$;

    // Canvas Variables
    canvas.element = undefined;
    canvas.context = undefined;

    // Pixel depth
    canvas.pixel = {
        width  : 5,
        height : 5
    };
    canvas.side_pixels = 75;

    // Default color
    canvas.default_color = "#FF0000";

    // Create the Canvas
    canvas.init = function(id) {
        if (!this.element) {
            this.element = document.getElementById(id);
        }
        this.element.width  = this.pixel.width  * this.side_pixels;
        this.element.height = this.pixel.height * this.side_pixels;
        this.context = this.element.getContext("2d");
    };

    // Clear the Canvas
    canvas.clear = function() {
        this.context.clearRect(0, 0, this.element.width, this.element.height);
    };

    canvas.draw = function (obj, backwards, rainbow, change) {
        if (!(obj && obj.length)) return;
        var pos, width, height, posx, pyrn, posy;
        var i, k;
        var obji;
        var color = obj[0];
        if (typeof color !== "string") {
            for (i = 0; i < obj.length; i++) {
                this.draw(obj[i], backwards, rainbow, change);
            }
            return;
        }
        if (change && change[color.toUpperCase()]) {
            color = change[color.toUpperCase()].color;
        }
        this.context.fillStyle = "#"+color || this.default_color;
        for (i = 1; i < obj.length; i+=3) {
            width  = this.pixel.width;
            height = this.pixel.height;
            pos    = obj[i];
            if (obj[i+1]) width  = obj[i+1] * this.pixel.width;
            if (obj[i+2]) height = obj[i+2] * this.pixel.height;
            posy   = pos/this.side_pixels;
            pyrn   = Math.round(posy);
            posx   = pos - (this.side_pixels*pyrn);
            posx   = ((posx < 0)? this.side_pixels + posx : posx ) * this.pixel.width;
            posy   = (pyrn - ((posy < pyrn)? 1 : 0)) * this.pixel.height;
            if (backwards) {
                posx = this.element.width - posx;
                width *= -1;
            }
            if (color === "clear") {
                this.context.clearRect(posx, posy, width, height);
            } else {
                if (rainbow) {
                    this.context.fillStyle = tuitpix.utils.newColor().hex;
                }
                this.context.fillRect(posx, posy, width, height);
            }
        }
    };

    // Converting an image into an object this way
    // reduces the ammount of elements considerably,
    // for example, if our matrix is of 75 side_pixels
    // the ammount of elements would be 75x75=5625,
    // with this, for simple draws it can become from 500 to 3000.
    // Also, allow us to handle colors as layers and add new layers.
    canvas.getData = function() {
        var size = this.side_pixels * this.side_pixels;
        var i, j, k;
        var x = 0;
        var y = 0;
        var _i, _x, _y;
        var width, height;
        var pixel;
        var color;
        var square;
        var space = " ";
        var comma = ",";
        var nl   = "\n";
        var obj  = [];
        var data = "";
LOOP:   for (i = 0; i < size; i++) {
            y = Math.floor(i / this.side_pixels);
            x = i - (y * this.side_pixels);
            pixel = this.context.getImageData(x, y, 1, 1).data;
            // If it's not transparent
            if (pixel[3]) {
                color = tuitpix.utils.RGBtoHex(pixel[0], pixel[1], pixel[2]);
                // Loop again through all the pixels to make sure this position isn't covered.
                for (j = 0; j < obj.length; j++) {
                    if (obj[j][0] === color) {
                        for (k = 1; k < obj[j].length; k+=3) {
                            _i = obj[j][k];
                            _y = Math.floor(_i / this.side_pixels);
                            _x = _i - (_y * this.side_pixels);
                            width  = obj[j][k+1];
                            height = obj[j][k+2];
                            if (_x <= x && (_x + width) > x && (_y + height) > y) {
                                continue LOOP;
                            }
                        }
                        square = this.getColorSquare(i, color);
                        // Clear this square to avoid to be injected in a later square.
                        this.context.clearRect(x, y, square.width-1, square.height-1);
                        obj[j].push(i, square.width, square.height);
                        continue LOOP;
                    }
                }
                square = this.getColorSquare(i, color);
                // Clear this square to avoid to be injected in a later square.
                this.context.clearRect(x, y, square.width-1, square.height-1);
                obj.push([color, i, square.width, square.height]);
            }
        }
        for (i = 0; i < obj.length; i++) {
            data += obj[i][0];
            for (j = 1; j < obj[i].length; j+=3) {
                _i     = obj[i][j];
                width  = obj[i][j+1];
                height = obj[i][j+2];
                if (width === 1) width = "";
                if (height === 1) height = "";
                data += space + _i;
                data += comma + width;
                data += comma + height;
            }
            data += nl;
        }
        return { obj: obj, data: data };
    };

    // Get Nearest Square
    canvas.getColorSquare = function(pos, color) {
        var width;
        var height;
        var pixel;
        var _color;
        var y = Math.floor(pos / this.side_pixels);
        var x = pos - (y * this.side_pixels);
        var _y = y;
        var _x = x;
        var __x, __y;
        var do_y = false;
        var keep = false;
DO:     do {
            if (!keep) do_y = !do_y;
            if (do_y) _y++;
            else      _x++;
            pixel = this.context.getImageData(_x, _y, 1, 1).data;
            // If empty, break.
            if (!pixel[3]) break;
            _color = tuitpix.utils.RGBtoHex(pixel[0], pixel[1], pixel[2]);
            if (color === _color) {
                // Be sure that it's a same-color row
                if (do_y && _x > x) {
                    __x = _x-1;
                    while (__x >= x) {
                        pixel  = this.context.getImageData(__x, _y, 1, 1).data;
                        if (!pixel[3]) break DO;
                        _color = tuitpix.utils.RGBtoHex(pixel[0], pixel[1], pixel[2]);
                        if (color !== _color) {
                            if (keep) {
                                if (!do_y) _y--;
                                continue DO;
                            }
                            break;
                        }
                        __x--;
                    }
                }
                // Be sure that it's a same-color column
                if (!do_y && _y > y) {
                    __y = _y-1;
                    while (__y >= y) {
                        pixel  = this.context.getImageData(_x, __y, 1, 1).data;
                        if (!pixel[3]) break DO;
                        _color = tuitpix.utils.RGBtoHex(pixel[0], pixel[1], pixel[2]);
                        if (color !== _color) {
                            if (keep) {
                                if (do_y) _x--;
                                continue DO;
                            }
                            break;
                        }
                        __y--;
                    }
                }
            }
            if (!keep && color !== _color) {
                if (do_y) _y--;
                else      _x--;
                do_y = !do_y;
                keep = true;
                _color = color;
            }
        } while(color === _color);
        if (do_y) _y--;
        else      _x--;
        return {
            // x  : x,
            // y  : y,
            // _x : _x,
            // _y : _y,
            // color  : color,
            // _color  : _color,
            width  : (_x - x) + 1,
            height : (_y - y) + 1
        };
    };

    canvas.newMini = function() {
        var mini = {};
        mini.$element = $("<canvas/>");
        mini.element  = mini.$element[0];
        mini.pixel    = {
            width  : 1,
            height : 1
        };
        mini.side_pixels = canvas.side_pixels;
        canvas.init.apply(mini);
        mini.clear  = canvas.clear.bind(mini);

        var draw = canvas.draw.bind(mini);
        mini.draw = function (obj, avatar, feature) {
            if (!avatar) return draw(obj, undefined, undefined, tuitpix.avatar.changed_skin_colors);
            feature = feature.split("-")[0];
            mini.clear();
            var body = avatar.selected.body;
            var selected;
            var feature_ready;
            for (var k, i = 0; i < avatar.order.length; i++) {
                k = avatar.order[i];
                if (k === "maker") return;
                if (k.indexOf(feature) !== -1) {
                    selected = obj;
                } else {
                    selected = avatar.features[k][body][avatar.selected[k]];
                }
                if (k !== "bodytype") {
                    draw(selected, undefined, undefined, avatar.changed_skin_colors);
                } else {
                    draw(selected);
                }
            }
        };

        mini.getData = canvas.getData.bind(mini);
        mini.getColorSquare = canvas.getColorSquare.bind(mini);
        mini.appendTo = function(element) {
            mini.$element.appendTo(element);
        };
        return mini;
    };

};
