// Copyright (c) 2013 Daniel Rodriguez
// MIT License [http://www.opensource.org/licenses/mit-license.php]

/* global $       */
/* global store   */
/* global jscolor */
(function(parent) {
    "use strict";

    // Our main variable
    var tuitpix = parent.tuitpix;

    // Store the libraries
    tuitpix.lib         = {};
    tuitpix.lib.$       = $;
    tuitpix.lib.store   = store;
    tuitpix.lib.jscolor = jscolor;

    // --- DEVELOPMENT ---
    // This lines will be deleted in the minified version.
    // --- DEVELOPMENT ---

    // --- PRODUCTION ---
    // // This lines will remain in the minified version.
    // // Except for the comments!
    // --- PRODUCTION ---

    tuitpix.main = function() {
        tuitpix.utils();  // tuitpix/utils.js
        tuitpix.ui();     // tuitpix/ui.js
        tuitpix.events(); // tuitpix/events.js
        tuitpix.canvas(); // tuitpix/canvas.js
        tuitpix.avatar(); // tuitpix/avatar.js
        tuitpix.maker();  // tuitpix/maker.js
        tuitpix.user();   // tuitpix/user.js

        // Start the canvas.
        tuitpix.canvas.init("avatar-canvas");

        // Draw the default avatar.
        tuitpix.ui.menu_body.click();
    };
})(this);
