// tuitpix/user.js
// Copyright (c) 2013 Daniel Rodriguez
// MIT License [http://www.opensource.org/licenses/mit-license.php]
//
// Where all the general-use functions are created.
//

/* global tuitpix */
tuitpix.user = function() {
    "use strict";

    var tuitpix = this;
    var user = tuitpix.user = {};
    var $    = tuitpix.lib.$;

    // User current selection
    user.current = {
        body   : 0,
        outfit : 0,
        hair   : 0,
        beard  : 0,
        addons : 0
    };

};
