#!/usr/bin/env node

// Makefile.dryice.js
//
// It will generate all the minified JavaScripts, CSS and HTML.
//
// Use it with: ./Makefile.dryice.js
//

var fs   = require("fs");
var path = require("path");
var exec = require('child_process').exec;
var copy = require("dryice").copy;
var uglifycss = require("uglifycss");
var uglifyjs  = require("uglify-js");
var _btoa     = require("btoa");

var year = new Date().getFullYear();

var license = [
    "TUITPIX, Pixel Art Avatar Maker.",
    "Copyright (c) " + year + " Daniel Rodríguez (@sadasant).",
].join("\n");

if (!fs.existsSync) {
    fs.existsSync = path.existsSync;
} else {
    path.existsSync = fs.existsSync;
}

console.log("Drying index.html into index.min.html");
copy({
    source : "index.html",
    dest   : "index.min.html",
    filter : [
        changeHTMLComments,
        htmlCompressor,
        function(data) {
            return [
                "<!--", license, "-->",
                data
            ].join("\n");
        }
    ]
});

console.log("Drying tuitpix styles");
var css = copy.createDataObject();
copy({
    source : {
        root    : "assets/lib",
        include : /.*\.css$/
    },
    dest   : css
});
copy({
    source : {
        root    : "assets/css",
        include : /.*\.css$/
    },
    dest   : css
});
copy({
    source : css,
    dest   : "assets-build/tuitpix.css",
    filter : function(data) {
        return [
            "/*", license, "*/",
            uglifycss.processString(data, { uglyComments : true })
        ].join("\n");
    }
});

console.log("Drying tuitpix core into assets/js-min/tuitpix.js");
var js = copy.createDataObject();
copy({
    source : {
        root    : "assets/lib",
        include : /.*\.js/
    },
    dest   : js
});
copy({
    source : {
        root : "assets/tuitpix"
    },
    dest   : js,
});
copy({
    source : js,
    dest   : "assets-build/tuitpix.js",
    filter : [
        changeJSComments,
        function(data) {
            return "/*\n" + license + "\n*/\n" +
                ";(function() {this.tuitpix = {};" +
                uglifyjs.minify(data, { fromString : true }).code +
                ";tuitpix.main();})();";
        }
    ]
});

console.log("Drying the PX files");
exec("rm -r "+__dirname +"/assets-build/px/*", function() {
    fs.readdir(__dirname +"/assets/px", function(err, list) {
        if (err) throw err;
        list.forEach(function(e, i) {
            // console.log(e);
            copy({
                source : "assets/px/" + e,
                dest   : "assets-build/px/" + e,
                filter : function(data) {
                    return _btoa(data.split("\n").filter(function(l) {
                        // Filtering comments
                        return l[0] !== "#";
                    }).join("\n"));
                }
            });
        });
    });
});

// changeHTMLComments
// removes the content between the <!--DEVELOPMENT--> tags,
// and uncomments the content within <!--PRODUCTION \n* PRODUCTION-->
function changeHTMLComments(data) {
    return (data
        .replace(/<!\-\-DEVELOPMENT[\d\D]*?DEVELOPMENT\-\->/g, "")
        .replace(/PRODUCTION\-\->|<!\-\-PRODUCTION/g, "")
    );
}

// htmlCompressor
// removes all the spaces, tabs and new-lines.
function htmlCompressor(data) {
    return (data
        .replace(/[\n\t]/g, " ")
        .replace(/ +/g, " ")
    );
}

// changeJSComments
// removes everything within `// --- DEVELOPMENT ---`
//
//      // --- DEVELOPMENT ---
//      var like = this;
//      // --- DEVELOPMENT ---
//
//      To this:
//
//
//
// And uncomments everything within `// --- PRODUCTION ---`
//
//      // --- PRODUCTION ---
//      // var like = this;
//      // --- PRODUCTION ---
//
//      To this:
//
//      var like = this;
//
function changeJSComments(data) {
    data = data.split("\n");
    var production_data = [];
    var in_development = false;
    var in_production = false;
    for (var i = 0; i < data.length; i++) {
        if (data[i].match(/\/\/ --- DEVELOPMENT ---/)) {
            in_development = !in_development;
            continue;
        }
        if (data[i].match(/\/\/ --- PRODUCTION ---/)) {
            in_production = !in_production;
            continue;
        }
        if (in_development) {
            continue;
        }
        if (in_production) {
            production_data.push(data[i].replace(/^ *\/\//, ""));
        } else {
            production_data.push(data[i]);
        }
    }
    return production_data.join("\n");
}
