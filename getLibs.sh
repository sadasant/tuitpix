function get() {
    echo $1
    wget -q -O $@
}

echo
echo TUITPIX JS BROWSER LIBRARIES
cd assets/lib/; rm *.*
get normalize.css        http://necolas.github.io/normalize.css/2.1.3/normalize.css
get jquery.js            http://code.jquery.com/jquery-1.10.2.min.js
get jquery.slimscroll.js https://raw.github.com/rochal/jQuery-slimScroll/9049771db559d8e89426df154aa1a065b00fc61b/jquery.slimscroll.js
get jscolor.js           http://jscolor.com/example/jscolor/jscolor.js
get store.js             https://raw.github.com/marcuswestin/store.js/master/store.min.js
get hs.png               http://jscolor.com/example/jscolor/hs.png
get hv.png               http://jscolor.com/example/jscolor/hv.png
get arrow.gif            http://jscolor.com/example/jscolor/arrow.gif
get cross.gif            http://jscolor.com/example/jscolor/cross.gif
cd ../../
