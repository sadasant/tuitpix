package main

import (
	"bitbucket.org/sadasant/tuitpix/server"
)

func main() {
	server.Start()
}
