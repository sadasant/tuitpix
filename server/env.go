package server

import (
	"os"
)

// Default values
var ENV = map[string]string{
	"PORT" : "80",
	"ENV"  : "development",
}

func parseENV() {
	for k, _ := range ENV {
		e := os.Getenv(k)
		if e != "" {
			ENV[k] = e
		}
	}
}
