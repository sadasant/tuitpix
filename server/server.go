package server

import (
	"fmt"
	"log"
	"net/http"
)

const (
	APP_NAME    = "TUITPIX"
	APP_VERSION = "v0.0.0"
)

func Start() {

	parseENV()

	http.Handle("/img/", staticHandler("/img", "assets/img/"))

	switch ENV["ENV"] {
	case "development":
		http.Handle("/assets/", staticHandler("/assets", "assets/"))
		http.Handle("/", singleHandler("index.html"))
		break
	case "production":
		http.Handle("/assets/px/", XSRFCheck(staticHandler("/assets/px/", "assets-build/px/")))
		http.Handle("/assets/tuitpix.js", singleHandler("assets-build/tuitpix.js"))
		http.Handle("/assets/tuitpix.css", singleHandler("assets-build/tuitpix.css"))
		http.Handle("/", templateHandler("index.min.html"))
		break
	}

	// Wake up the web server
	fmt.Println(APP_NAME, APP_VERSION, "starting at 127.0.0.1:"+ENV["PORT"])
	err := http.ListenAndServe(":"+ENV["PORT"], mainHandler(http.DefaultServeMux))
	if err != nil {
		log.Fatal(err)
	}
}
