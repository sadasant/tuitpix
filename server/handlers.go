package server

import (
	"code.google.com/p/xsrftoken"
	"github.com/gorilla/sessions"
	"html/template"
	"log"
	"net/http"
)

var store = sessions.NewCookieStore([]byte("ax;gn=j1v.umo2o:vBIHNu4DPQKJ<h"))
var XSRF_key = "VpIg=4ZDtMJhL7Wcgb,OLmjBO:L+:T"

// Static Directory Handler
func staticHandler(prefix, dir string) http.Handler {
	return http.StripPrefix(prefix, http.FileServer(http.Dir(dir)))
}

// Single File Handler
func singleHandler(filename string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, filename)
	})
}

// Template File Handler
func templateHandler(filename string) http.Handler {
	tmpl := template.Must(template.New(filename).ParseFiles(filename))
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := store.Get(r, filename)
		if err != nil {
			log.Printf("Error with \"%s\" session: %v", filename, err)
			errorHandler(w, r, 305)
			return
		}
		tmpl.Execute(w, session.Values)
	})
}

// Intercepts the http handler and checks wether the XSRF body value is set and is correct.
func XSRFCheck(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		index_path := "index.min.html"
		index_session, err := store.Get(r, index_path)
		if err != nil {
			log.Printf("Error with \"%s\" session: %v", index_path, err)
			errorHandler(w, r, 305)
			return
		}
		xsrf, ok := index_session.Values["XSRF"].(string)
		if !(ok && xsrf == r.FormValue("XSRF") &&
			xsrftoken.Valid(xsrf, XSRF_key, index_session.ID, index_path)) {
			errorHandler(w, r, 401)
			return
		}
		handler.ServeHTTP(w, r)
	})
}

// Logs the requests, creates the sessions and continues with the next handler.
func mainHandler(handler http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Log
		log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL)

		// Set the XSRF session for the index
		switch ENV["ENV"] {
		case "production":
			index_path := "index.min.html"
			index_session, err := store.Get(r, index_path)
			if err != nil {
				log.Printf("Error with \"%s\" session: %v", index_path, err)
				errorHandler(w, r, 305)
				return
			}
			xsrf, ok := index_session.Values["XSRF"].(string)
			if !(ok && xsrf != "" &&
				xsrftoken.Valid(xsrf, XSRF_key, index_session.ID, index_path)) {
				index_session.Values["XSRF"] = xsrftoken.Generate(XSRF_key, index_session.ID, index_path)
				sessions.Save(r, w)
			}
			break
		}

		// Resume
		handler.ServeHTTP(w, r)
	}
}

// Error handler, 404 and related
// Responds according to the given status
func errorHandler(w http.ResponseWriter, r *http.Request, status int) {
	w.WriteHeader(status)
	log.Printf("%s %s %s %v", r.RemoteAddr, r.Method, r.URL, status)
	if status == http.StatusNotFound {
		w.Header().Set("Content-Type", "text/plain")
		w.Write([]byte("404"))
	}
}
